import React, { Component } from 'react';
import {Table, Button} from 'reactstrap';

import axios from 'axios';

class App extends Component {
   
    constructor(props){
        super(props);
        this.state = {
            id: '',               
            modelname: '',             
            color: '',            
            productionyear: '',
            price: '',     
        
            allcars: []        
        };
        
        this.handleSubmit = this.handleSubmit.bind(this);    
        this.handleChange = this.handleChange.bind(this);    
        this.handleDelete = this.handleDelete.bind(this);          
        this.deleteCar = this.deleteCar.bind(this);     
        this.getCars();                                    
    }
    
    handleChange(event){

        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue,
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {id, modelname, color, productionyear, price} = this.state;
        await axios.put("https://tgd2kz674a.execute-api.eu-west-3.amazonaws.com/cars", {id: `${id}`, modelname: `${modelname}`, color: `${color}`, productionyear: `${productionyear}`, price: `${price}` });
        this.getCars();
    }
    
    async handleDelete(event) {
        event.preventDefault();
        const id = this.state.id;
        await axios.delete('https://tgd2kz674a.execute-api.eu-west-3.amazonaws.com/cars/'+id);
        this.getCars();
    }

    async handleGet(event) {
        event.preventDefault();
        const id = this.state.id;
        const data = await axios.get('https://tgd2kz674a.execute-api.eu-west-3.amazonaws.com/cars/'+id).then(response => response.data);
        this.setState({id: data.Item.id, modelname: data.Item.modelname, color: data.Item.color, productionyear: data.Item.productionyear, price: data.Item.price});
        this.getCars();
    }

    async deleteCar(id) {
        await axios.delete('https://tgd2kz674a.execute-api.eu-west-3.amazonaws.com/cars/'+id);
        this.getCars();
    }

    async getCars() {
        const data = await axios.get("https://tgd2kz674a.execute-api.eu-west-3.amazonaws.com/cars").then(response => response.data);
        this.setState({allcars: data.Items});
    }

    render() {
    
        return (

       
        <div className="main">

            <h1 align="center"> CRUD cars </h1>
            
            <br/>
            <h3> cars </h3>
        
        <Table responsive striped bordered hover>
            <thead>
                <th> id </th>
                <th> modelname </th>
                <th> color </th>
                <th> prodictionyear </th>
                <th> price </th>
            </thead>

            <tbody>
                {this.state.allcars.map((item) => (
                    <tr key={item.id}>
                        <td> {item.id} </td>
                        <td> {item.modelname} </td>
                        <td> {item.color} </td>
                        <td> {item.productionyear} </td>
                        <td> {item.price} </td>
                        <td> <Button onClick={() => this.deleteCar(item.id)}> Delete </Button></td>
                    </tr>
                ))}
            </tbody>
        </Table>
        
            <div class ="add/delete/get a car">
                    
                <h3> Add a car </h3>
                    
                <form onSubmit={this.handleSubmit}>
                        
                    <label> id </label>
                    <input
                    type="text"
                    name="id"
                    onChange={this.handleChange}
                    value={this.state.id}
                    />
                        
                    <label> modelname </label>
                    <input
                    type="text"
                    name="modelname"
                    onChange={this.handleChange}
                    value={this.state.modelname}
                    />
                
                    <label> color </label>
                    <input
                    type="text"
                    name="color"
                    onChange={this.handleChange}
                    value={this.state.color}
                    />
                        
                    <label> productionyear </label>
                    <input
                    type="text"
                    name="productionyear"
                    onChange={this.handleChange}
                    value={this.state.productionyear}
                    />
                    <label> price </label>
                    <input
                    type="text"
                    name="price"
                    onChange={this.handleChange}
                    value={this.state.price}
                    />
                        
                    <button type="submit"> Save </button>
                </form>
                        
                <br/>
                    
                <h3> Delete a car </h3>
                    
                <form onSubmit={this.handleDelete}>
                    <label> id </label>
                    <input
                    type="text"
                    name="id"
                    onChange={this.handleChange}
                    value={this.state.id}
                    />
                    
                    <button type="submit"> Delete </button>
                </form>
                    
                <br/>
                     
            </div>
        
        </div>
        );
    }
}
 
export default App;